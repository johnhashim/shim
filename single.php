<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package shim
 */

get_header(); ?>

	<div class="display-flex grid-wrapper">
		<main id="main" class="site-main">
		<div class=" container portfolio">
			<div class="row">
				<div class="full-width">
					<div class="heading">				
						<img src="https://image.ibb.co/cbCMvA/logo.png" />
					</div>
				</div>	
			</div>
			<div class="bio-info">

			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', get_post_format() );

				the_post_navigation();

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</div>
		</main><!-- #main -->
	</div><!-- .grid-wrapper -->
<?php get_footer(); ?>
