<?php
/**
 * Customizer sections.
 *
 * @package shim
 */

/**
 * Register the section sections.
 *
 * @author WDS
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function shim_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'shim_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'shim' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'shim_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'shim' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'shim' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'shim_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'shim' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'shim_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'shim' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'shim_customize_sections' );
