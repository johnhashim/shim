<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package shim
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'shim' ); ?></h2>
</section>
