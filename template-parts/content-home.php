<?php
/**
 * Template part for displaying homepage acf.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package shim
 */

?>

	<div class="full-width">
			<?php
				the_content();

				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'shim' ),
						'after'  => '</div>',
					)
				);
			?>
	</div><!-- #post-## -->
