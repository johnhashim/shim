<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, shim_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package shim
 */

get_header(); ?>

	<main id="main" class="site-main container">

		<?php do_action( 'shim_scaffolding_content' ); ?>

	</main><!-- #main -->

<?php get_footer(); ?>
